<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectSkkl extends Model
{
    use HasFactory;

    protected $table = 'project_skkl';
}
