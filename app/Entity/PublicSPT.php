<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicSPT extends Model
{
    use HasFactory;

    protected $table = 'public_spt';

}
