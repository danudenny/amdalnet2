<?php

namespace App\Http\Controllers;

use App\Entity\EnvManageDoc;
use Illuminate\Http\Request;

class EnvManageDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\EnvManageDoc  $envManageDoc
     * @return \Illuminate\Http\Response
     */
    public function show(EnvManageDoc $envManageDoc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\EnvManageDoc  $envManageDoc
     * @return \Illuminate\Http\Response
     */
    public function edit(EnvManageDoc $envManageDoc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\EnvManageDoc  $envManageDoc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnvManageDoc $envManageDoc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\EnvManageDoc  $envManageDoc
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnvManageDoc $envManageDoc)
    {
        //
    }
}
