export default {
  route: {
    dashboard: 'Dasbor',
    introduction: 'Pengantar',
    documentation: 'Dokumentasi',
    guide: 'Panduan',
    permission: 'Perizinan',
    pagePermission: 'Perizinan Halaman',
    rolePermission: 'Perizinan Peran',
    directivePermission: 'Arahan',
    icons: 'Ikon',
    components: 'Komponen',
    componentIndex: 'Pengenalan',
    tinymce: 'Tinymce',
    markdown: 'Markdown',
    jsonEditor: 'Editor JSON',
    dndList: 'Daftar Dnd',
    splitPane: 'SplitPane',
    avatarUpload: 'Unggah Avatar',
    dropzone: 'Zona Drop',
    sticky: 'Sticky',
    countTo: 'CountTo',
    componentMixin: 'Mixin',
    backToTop: 'Kembali ke atas',
    dragDialog: 'Dialog Tarik',
    dragSelect: 'Pilih Tarik',
    dragKanban: 'Kanban Tarik',
    charts: 'Grafik',
    keyboardChart: 'Grafik Keyboard',
    lineChart: 'Grafik Garis',
    mixChart: 'Grafik Campur',
    example: 'Contoh',
    nested: 'Rute Bersarang',
    menu1: 'Menu 1',
    'menu1-1': 'Menu 1-1',
    'menu1-2': 'Menu 1-2',
    'menu1-2-1': 'Menu 1-2-1',
    'menu1-2-2': 'Menu 1-2-2',
    'menu1-3': 'Menu 1-3',
    menu2: 'Menu 2',
    table: 'Tabel',
    dynamicTable: 'Tabel Dinamis',
    dragTable: 'Tabel Tarik',
    inlineEditTable: 'Sunting Ditempat',
    complexTable: 'Tabel Kompleks',
    treeTable: 'Tabel Pohon',
    customTreeTable: 'Kustom Tabel Pohon',
    tab: 'Tab',
    form: 'Formulir',
    createArticle: 'Buat Artikel',
    editArticle: 'Sunting Artikel',
    articleList: 'Artikel',
    errorPages: 'Halaman Error',
    page401: '401',
    page404: '404',
    errorLog: 'Log Error',
    excel: 'Excel',
    exportExcel: 'Export Excel',
    selectExcel: 'Export Terpilih',
    mergeHeader: 'Gabung Header',
    uploadExcel: 'Unggah Excel',
    zip: 'Zip',
    pdf: 'PDF',
    exportZip: 'Export Zip',
    theme: 'Tema',
    clipboardDemo: 'Clipboard',
    i18n: 'I18n',
    externalLink: 'Tautan Eksternal',
    elementUi: 'Element UI',
    administrator: 'Administrator',
    users: 'Pengguna',
    userProfile: 'Profile Pengguna',
    project: 'Kegiatan',
    listProject: 'Daftar Kegiatan',
    listProjectPre: 'Belum Pengajuan PL',
    listProjectPost: 'Dalam Pengajuan PL',
    listProjectProcess: 'Dalam Pengujian',
    listProjectIssued: 'Sudah Terbit PL',
    addProject: 'Tambah Kegiatan',
    publishProject: 'Publikasi Kegiatan',
    initiator: 'Pemrakarsa',
    formulator: 'Penyusun',
    masterData: 'Data Master',
    provinsi: 'Provinsi',
    lpjp: 'LPJP',
    expertBank: 'Bank Ahli',
    formulatorExpert: 'Tenaga Ahli Penyusun',
    addFormulator: 'Tambah Penyusun',
    editFormulator: 'Edit Penyusun',
    luk: 'LUK',
    configuration: 'Konfigurasi Kegiatan',
    componentActivities: 'Komponen Kegiatan',
    addRona: 'Tambah Rona Lingkungan',
    ronaAwal: 'Rona Awal',
    sop: 'SOP',
    addSop: 'Tambah SOP',
    cluster: 'Cluster',
    formulatorTeam: 'Tim Penyusun',
    tuk: 'Tim Uji Kelayakan',
    editWorkspace: 'Edit Workspace',
    projectWorkspace: 'Project workspace',
  },
  navbar: {
    logOut: 'Keluar',
    dashboard: 'Dasbor',
    github: 'Github',
    theme: 'Tema',
    size: 'Ukuran Global',
    profile: 'Profile',
  },
  login: {
    title: 'Masuk ke Dasbor Anda',
    logIn: 'Masuk',
    username: 'Nama pengguna',
    password: 'Password',
    any: 'apa saja',
    thirdparty: 'Terhubung dengan',
    thirdpartyTips: 'Tidak bisa disimulasikan di lokal, silahkan gabungkan dengan simulasi yang Anda miliki!',
    email: 'Surel',
    userType: 'Jenis User',
    registrationForm: 'Form Pendaftaran',
    address: 'Alamat',
    phone: 'Nomor Telepon',
    pic: 'Penanggung Jawab',
    name: 'Nama',
  },
  documentation: {
    documentation: 'Dokumentasi',
    laravel: 'Laravel',
    github: 'Github Repository',
  },
  permission: {
    addRole: 'Peran Baru',
    editPermission: 'Sunting Perizinan',
    roles: 'Peran Anda',
    switchRoles: 'Mengalihkan peran',
    tips: 'Dalam beberapa kasus, tidak cocok menggunakan v-role/v-permission, seperti pada komponen Tab atau el-table-column dan asyncrhonous dom rendering, dalam hal ini dapat dicapai hanya dengan setting v-if dengan checkRole dan/atau checkPermission.',
    delete: 'Hapus',
    confirm: 'Konfirmasi',
    cancel: 'Batal',
  },
  guide: {
    description: 'The guide page is useful for some people who entered the project for the first time. You can briefly introduce the features of the project. Demo is based on ',
    button: 'Tampilkan Panduan',
  },
  components: {
    documentation: 'Dokumentasi',
    tinymceTips: 'Rich text editor is a core part of management system, but at the same time is a place with lots of problems. In the process of selecting rich texts, I also walked a lot of detours. The common rich text editors in the market are basically used, and the finally chose Tinymce. See documentation for more detailed rich text editor comparisons and introductions.',
    dropzoneTips: 'Because my business has special needs, and has to upload images to qiniu, so instead of a third party, I chose encapsulate it by myself. It is very simple, you can see the detail code in @/components/Dropzone.',
    stickyTips: 'when the page is scrolled to the preset position will be sticky on the top.',
    backToTopTips1: 'When the page is scrolled to the specified position, the Back to Top button appears in the lower right corner',
    backToTopTips2: 'You can customize the style of the button, show / hide, height of appearance, height of the return. If you need a text prompt, you can use element-ui el-tooltip elements externally',
    imageUploadTips: 'Since I was using only the vue@1 version, and it is not compatible with mockjs at the moment, I modified it myself, and if you are going to use it, it is better to use official version.',
  },
  table: {
    description: 'Deskripsi',
    dynamicTips1: 'Fixed header, sorted by header order',
    dynamicTips2: 'Not fixed header, sorted by click order',
    dragTips1: 'The default order',
    dragTips2: 'The after dragging order',
    name: 'Nama',
    title: 'Judul',
    importance: 'Imp',
    type: 'Type',
    remark: 'Catatan',
    search: 'Cari',
    add: 'Tambah',
    export: 'Export',
    reviewer: 'reviewer',
    id: 'ID',
    date: 'Tanggal',
    author: 'Pengarang',
    readings: 'Bacaan',
    status: 'Status',
    actions: 'Aksi',
    edit: 'Edit',
    publish: 'Terbit',
    draft: 'Draf',
    delete: 'Hapus',
    cancel: 'Batal',
    confirm: 'Konfirmasi',
    keyword: 'Katakunci',
    role: 'Peran',
  },
  addNode: 'Tambah Node',
  getTree: 'Ambil Struktur Pohon',
  errorLog: {
    tips: 'Please click the bug icon in the upper right corner',
    description: 'Now the management system are basically the form of the spa, it enhances the user experience, but it also increases the possibility of page problems, a small negligence may lead to the entire page deadlock. Fortunately Vue provides a way to catch handling exceptions, where you can handle errors or report exceptions.',
    documentation: 'Document introduction',
  },
  excel: {
    export: 'Export',
    selectedExport: 'Export Selected Items',
    placeholder: 'Please enter the file name(default excel-list)',
  },
  zip: {
    export: 'Export',
    placeholder: 'Please enter the file name(default file)',
  },
  pdf: {
    tips: 'Here we use window.print() to implement the feature of downloading pdf.',
  },
  theme: {
    change: 'Change Theme',
    documentation: 'Theme documentation',
    tips: 'Tips: It is different from the theme-pick on the navbar is two different skinning methods, each with different application scenarios. Refer to the documentation for details.',
  },
  tagsView: {
    refresh: 'Refresh',
    close: 'Tutup',
    closeOthers: 'Tutup Lainnya',
    closeAll: 'Tutup Semua',
  },
  settings: {
    title: 'Page style setting',
    theme: 'Theme Color',
    tagsView: 'Open Tags-View',
    fixedHeader: 'Fixed Header',
    sidebarLogo: 'Sidebar Logo',
  },
  user: {
    'role': 'Peran',
    'password': 'Password',
    'confirmPassword': 'Konfirmasi password',
    'name': 'Nama',
    'email': 'Surel',
  },
  roles: {
    description: {
      admin: 'Super Administrator. Memiliki akses ke semua halaman.',
      manager: 'Manager. Memiliki akses hampir ke semua halaman kecuali perizinan.',
      editor: 'Editor. Memiliki perizinan penuh untuk artikel.',
      user: 'Normal user. Memiliki akses ke sebagian halaman',
      visitor: 'Visitor. Memiliki akses ke halaman static, tidak memiliki izin menulis data',
      initiator: 'Pemrakarsa proyek / kegiatan, yang akan di lakukan pemeriksaan dampak lingkungannya',
      formulator: 'Penyusun dokumen analisis dampak lingkungan',
      institution: 'Lembaga Penyedia Jasa Penyusunan analisis dampak lingkungan',
      'admin-standard': 'Pusat Standardisasi Lingkungan dan Kehutanan',
      examiner: 'Penguji dalam pemeriksaan analisis dampak lingkungan',
      'examiner-institution': 'Lembaga Uji Kelayakan',
      'admin-system': 'Administrator system',
      'admin-central': 'Administrator pusat Kementrian Lingkungan Hidup dan Kehutanan',
      'admin-regional': 'Administrator daerah Kementrian Lingkungan Hidup dan Kehutanan',
      public: 'Publik',
    },
  },
};
